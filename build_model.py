import myNLP.reader as reader 
import pickle
import numpy as np
import tensorflow as tf
import keras
from keras import Model
from keras.layers import Input
from keras.layers import Dense
import random

def load_model(nb_features, reg_value=None):

    OUTPUT_UNITS=2
    ACTIVATION_OUTPUT="softmax"
    
    input_layer = Input(batch_shape=(None, nb_features),
                        name="input_layer",
                        dtype="float32")

    #hl1=Dense(512, activation=ACTIVATION_OUTPUT, kernel_regularizer=keras.regularizers.l2(reg_value))(input_layer)

    output_layer=Dense(OUTPUT_UNITS,
                       activation=ACTIVATION_OUTPUT,
                       name="output_layer",
                       kernel_regularizer=keras.regularizers.l2(reg_value))(input_layer)

    model=Model(input_layer, output_layer)

    return model
    


    

def main( reg_val=1e-6, learning_rate=0.1, word_index=None, training_data=None ): #0.0001


    if word_index == None:
        word_index   = reader.load_pickle ('word_index.pkl')

    if training_data == None:
        training_data = reader.load_pickle("ucl_students_data_task_1/data_train.pkl")

        
    #shuffle training data into random order
    random.shuffle (training_data)

    labels  = [i[0] for i in training_data]
    strings = [i[1] for i in training_data]

    vector_strings = np.array([ reader.sentance_to_vector(word_index, string) for string in strings  ])
    
    y_train=keras.utils.to_categorical(labels)

    x_train=vector_strings

    model=load_model(len(word_index), reg_value=reg_val) #0.001

    opt=keras.optimizers.SGD(lr=learning_rate, momentum=0., decay=0., nesterov=False)

    model.compile(optimizer=opt, loss="binary_crossentropy", metrics=["accuracy"])

    print (model.summary())

    history=model.fit(x=x_train, y=y_train, batch_size=64, nb_epoch=20)

    #Get test data and test performance 
    
    test_data= pickle.load(open ("ucl_students_data_task_1/data_test.pkl", "rb"))
    test_strings = [datum[1] for datum in test_data]
    test_labels  = np.array([datum[0] for datum in test_data])

    x_test = np.array([ reader.sentance_to_vector(word_index, string) for string in test_strings  ])

    y_test = keras.utils.to_categorical(test_labels)

    model.save("my_model.h5")

    preds=model.predict(x_test)
    #print (preds)
    #print (labels)

    with tf.Session() as sess:
        preds = keras.backend.argmax(preds).eval(session=sess)
        print ( tf.contrib.metrics.f1_score(test_labels, preds))


    total       = len(preds)
    total_wismo = sum(test_labels)
    total_OI    = total - total_wismo

    print ("\n")
    print ("total wismo entries: " + str(total_wismo))
    print ("total OI entries: " + str(total_OI))

    print ("total classifies as WISMO: " + str(sum(preds)))
    print ("total classified as OI: " + str( total - sum(preds)) )

    eff_den = 0
    eff_num = 0



    for label_i in range(len(test_labels)):

        if test_labels[label_i] == 1:
            eff_den +=1

            if preds[label_i] == 1:
                eff_num +=1

    efficiency = eff_num / eff_den
    print ("efficiency: " + str (efficiency))

    pure_den = 0
    pure_num = 0

    for label_i in range(len(preds)):
        if preds[label_i] == 1:
            pure_den +=1

            if test_labels[label_i] == 1:
                pure_num +=1

    purity = 0
    try:
        purity = pure_num / pure_den
        print ("purity: " + str (purity))

    except:
        print ("purity: 0")

    f_1 = (2 * purity * efficiency) / (purity + efficiency)
    print ("F1: " + str (f_1))
    
    return efficiency, purity, f_1

if __name__ == "__main__":
    main()
