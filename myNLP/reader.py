import pickle
import myNLP.word_tokenize as wtokenize
import pandas as pd 
import numpy as np


def get_tokenized_list(training_data):

    #labels  = [datum[0] for datum in training_data]
    strings = [datum[1] for datum in training_data]


    tokenized_list = [ wtokenize.tokenize(string) for string in strings ]

    return tokenized_list


def cat_and_count (dictionary, tokenized_list):

    for sentance in tokenized_list:
        for word in sentance:
            if word not in dictionary.keys():
                dictionary[word]=1
            else:
                dictionary[word]+=1



def load_pickle(path="ucl_students_data_task_1/data_train.pkl"):

    training_data=pickle.load(open(path, "rb"))

    return training_data

def build_word_index(path="ucl_students_data_task_1/data_train.pkl"):
    """
    create pandas series to record and count tokens,
    + simple numerical operations
    """

    training_data  = load_pickle(path)
    tokenized_list = get_tokenized_list (training_data)
    word_dict = pd.Series()

    cat_and_count(word_dict, tokenized_list)


    #discard lowest appearing 10% of tokens
    size = len(word_dict)
    
    word_dict = word_dict.nlargest( int(size*0.95) )
    word_dict = word_dict.nsmallest( int(size*0.95) )

    #discard tokens that appear less than min_n times
    min_n = 5
    word_dict = word_dict[word_dict.ge(min_n)]

    list_dump = [key for key in word_dict.keys()]
    
    pickle_out = open('word_index.pkl', 'wb')
    pickle.dump(list_dump, pickle_out)
    
    return word_dict


def sentance_to_vector (word_index, sentance):
    """
    return normalised sentance vector based
    on word index
    """
    sentance = wtokenize.tokenize(sentance)
    
    vector = np.zeros (len(word_index))

    for word in sentance:

        try:
            index = word_index.index(word)
            vector[index]+=1
        except:
            '''
            print ('word "' + word + '" is not in index' )
            '''
            continue

    #normalise vector 

    '''
    vector_norm = vector.sum()

    if vector_norm > 0:
        vector = vector/vector_norm
    '''

    return vector 

        

def main():
    print ("loaded module")


if __name__ == "__main__":
    main()

main()
