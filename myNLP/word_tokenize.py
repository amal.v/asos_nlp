from nltk.tokenize import word_tokenize
from nltk.util import ngrams
import pandas as pd 
import numpy as np
import re


def tokenize (sentance):
    """
    given a string, turn into list of tokens
    """

    #remove lower case letters
    sentance = sentance.lower()
    
    sentance = word_tokenize(sentance)

    #swap every price with specific string
    sentance = [ word if not '£' in word else "generic_price" for word in sentance]

    #remove punctuation
    sentance = [ re.sub(r'[^a-z0-9_\s]', '', word) for word in sentance ]

    #now, get n_grams
    two_grams    = get_n_grams (sentance, 2)
    three_grams  = get_n_grams (sentance, 3)

    sentance += two_grams 
    #sentance += three_grams
    
    '''
    remove basic words and empty entries 
    '''
    word_list = ['']#, 'a','i', 'the', 'and', 'but']
    prepositions = ['on']#, 'at', 'the', 'by', 'i', 'to', 'my', 'and', 'it', 'for', 'this']
                    #'was', 'but', 'you', 'of', 'that', 'so', 'a', 'in', 'is']


    word_list+=prepositions 
    sentance = remove_specific_words(sentance, word_list)
    
    return sentance


def get_n_grams (sentance, n):

    n_grams = zip(*[sentance[i:] for i in range(n)])
    return [" ".join(n_gram) for n_gram in n_grams]

    
def remove_specific_words (sentance, remove_list):

    for remove in remove_list:
        sentance = [ word for word in sentance if word != remove ]

    return sentance
        

