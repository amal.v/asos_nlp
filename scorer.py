from keras.models import load_model
import myNLP.reader as reader
import sys 
import numpy as np
import pickle
import tensorflow as tf
import keras

def scorer (sentance):

    word_index   = pickle.load(open ('word_index.pkl', 'rb' ))
    model = load_model ("my_model.h5")

    tokenized = np.array([ reader.sentance_to_vector(word_index, sentance) ])

    preds=model.predict(tokenized)

    #only want to run the active tf session in the loop 
    with tf.Session() as sess:
        preds = keras.backend.argmax(preds).eval(session=sess)

    output = "OTHER_INTENT"
    if preds[0] == 1:
        output = "WISMO"
        
    print( output )
    return output
    

if __name__ == "__main__":
    scorer(sys.argv[1])
    
