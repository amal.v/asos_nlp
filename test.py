import myNLP.reader as reader 
import pickle


word_index = pickle.load(open ('word_index.pkl', 'rb' ))


sen1 = 'hi, i was wondering where my recent order was'
sen2 = 'my order is late and i dont know when it will arrive'
sen3 = 'i like pineapples and vaious other clothes, like socks'


array = [sen1, sen2, sen3,
         'i want to know where my trousers are',
         'where is my order',
         'when can i expect to recieve my shoes',
         'the shirts i bought smell like 5',
         'one of the short was torn, can i get a replacement',
         'this is completely unrelated']

for sen in array:
    print (' ')
    print (reader.tokenize(sen))
    vector = reader.sentance_to_vector (word_index, sen)
    print (vector)
    print (vector.sum())
