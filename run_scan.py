import build_model


reg_values = [1e3, 1e2 , 1e1, 1, 1e-1, 1e-2, 1e-3, 1e-4, 1e-5, 1e-6, 1e-7]
lr_values  = [1e3, 1e2 , 1e1, 1, 1e-1, 1e-2, 1e-3, 1e-4, 1e-5, 1e-6, 1e-7]

#reg_values = [0.001]
#lr_values = [0.1]

f = open("scan.txt", "w")

for reg in reg_values:

    for lr in lr_values:

        print ( "running values: " + str(reg) + " " + str(lr) )
        
        efficiency, purity = build_model.main (reg, lr)

        f.write("\n")
        f.write("reg value: " + str(reg) + ", learning rate: " + str(lr) + "\n")
        f.write("efficiency: " + str(efficiency) + ", purity: " + str(purity) )
